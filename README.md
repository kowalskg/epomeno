## Epemeno : Configuration Tool for IDSC

This Django Application is used to control the Deidentification Processing here at MCW and Frodtert for Research Purposes



### References 

[Anonomizer Doc](https://mircwiki.rsna.org/index.php?title=The_MIRC_DICOM_Anonymizer)
See the dicom-anonymizer.script in the ~/samples dir of this project

```
Kirk:
 
I didn't write any documentation. It's a command-line tool. It has a DicomAnonymizer and a DicomPixelAnonymizer in it.
 
Get DicomAnonymizerTool-installer.jar from the RSNA MIRC site. It works like all the other installers. It creates a folder called DicomAnonymizerTool containing all the necessary files.
 
The program to run is called DAT.jar.
 
When you run it with no parameters, it lists the command line parameters it accepts. It also checks the computer's configuration to see whether it has what it needs to run.
 
The program can anonymize a single file or a tree of files.
 
Here is an example invocation of the program:
 
  java –Xmx1000m –jar DAT.jar –in data –dp –dpa –v –n 5
 
That command runs the tool with a 1GB heap, it applies the DicomPixelAnonymizer and the DicomAnonymizer on all the DICOM files in the tree with the root directory called data. Because the –dp and –dpa parameters don't specify script files, the program uses the default scripts found in the DicomPixelAnonymizer directory with the program. Because there is no –out parameter, it creates an output folder called data-an for the anonymized files and reproduces the structure of the data directory. The –v tells the program to produce verbose output in the command window. The –n 5 tells the program to run 5 parallel threads to process the files.
 
JP
```

[DICOM standard](https://www.dicomstandard.org/current/)

### Documentation on XNAT 

Attributes support
Each resource level also has a set of metadata fields that can be informed. This set of fields depends on the resource level and on its type in the XNAT schema.

>>> # use hard-coded shortcuts from the REST API
>>> my_project.attrs.set('secondary_ID', 'myproject')
>>> my_project.attrs.get('secondary_ID')
'myproject'
>>> # use XPATH from standard or custom XNAT Schema
>>> my_project.attrs.set('xnat:projectData/keywords', 'test project')
>>> my_project.attrs.get('xnat:projectData/keywords')
'test project'
>>> # get or set multiple attributes in a single request to improve performance
>>> my_project.attrs.mset({'xnat:projectData/keywords':'test project', 'secondary_ID':'myproject'})
>>> my_project.attrs.mget(['xnat:projectData/keywords', 'secondary_ID'])
['test porject', 'myproject']
 
To get the searchable types and fields to put in the constraints, rows and columns parameters, use the Interface.inspect.datatypes method:

>>> central.inspect.datatypes(optional_filter)
[..., 'xnat:subjectData', 'xnat:projectData', 'xnat:mrSessionData',  ...]
>>> central.inspect.datatypes('xnat:subjectData', optional_filter)
['xnat:subjectData/SUBJECT_ID',
 'xnat:subjectData/INSERT_DATE',
 'xnat:subjectData/INSERT_USER',
 'xnat:subjectData/GENDER_TEXT',
 ...]
>
Fields : 
subject Fields :
['xnat:subjectData/INSERT_DATE', 
'xnat:subjectData/INSERT_USER', 
'xnat:subjectData/GENDER_TEXT', 
'xnat:subjectData/HANDEDNESS_TEXT', 
'xnat:subjectData/DOB', 
'xnat:subjectData/EDUC', 
'xnat:subjectData/SES', 
'xnat:subjectData/INVEST_CSV', 
'xnat:subjectData/PROJECTS', 
'xnat:subjectData/PROJECT', 
'xnat:subjectData/SUB_GROUP', 
'xnat:subjectData/ADD_IDS',
'xnat:subjectData/RACE', 
'xnat:subjectData/ETHNICITY']

Project Fields : 
[
'xnat:projectData/INSERT_DATE', 
'xnat:projectData/INSERT_USER', 
'xnat:projectData/ID',
'xnat:projectData/NAME', 
'xnat:projectData/NAME_CSV', 
'xnat:projectData/DESCRIPTION', 
'xnat:projectData/DESCRIPTION_CSV',
'xnat:projectData/SECONDARY_ID', 
'xnat:projectData/KEYWORDS', 
'xnat:projectData/PI',
'xnat:projectData/PROJECT_INVS', 
'xnat:projectData/PROJECT_ACCESS', 
'xnat:projectData/PROJECT_USERS', 
'xnat:projectData/PROJECT_OWNERS',
'xnat:projectData/PROJECT_MEMBERS', 
'xnat:projectData/PROJECT_COLLABS', 
'xnat:projectData/PROJECT_LAST_WORKFLOW',
'xnat:projectData/PROJECT_LAST_ACCESS', 
'xnat:projectData/PROJECT_FAV', 
'xnat:projectData/PROJ_MR_COUNT',
'xnat:projectData/PROJ_CT_COUNT', 
'xnat:projectData/PROJ_PET_COUNT', 
'xnat:projectData/PROJ_UT_COUNT'
]

See : https://wiki.xnat.org/display/XAPI/XNAT+REST+XML+Path+Shortcuts#XNATRESTXMLPathShortcuts-xnat:projectData

ShortCuts also : 
https://wiki.xnat.org/display/XAPI/Project+Data+REST+XML+Path+Shortcuts

Invistigator DUMP with Projects attached here , not on the project 

https://xnat.mcw.edu/xnat/xapi/investigators/4/

