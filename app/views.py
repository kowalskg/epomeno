from django.shortcuts import render, get_object_or_404, reverse
from django.urls import reverse_lazy
from django.template import loader
from django.utils import timezone

from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView

from app.models import Study, DeidParams, DicomElements, TemplateElements, DeidTemplate
import logging

logger = logging.getLogger(__name__)

# class Based Views
class StudyList(ListView):
    model = Study


class StudyDetail(DetailView):
    template_name = 'app/study_details.html'
    model = Study

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        o = context['object']
        # Set this for use on the creation of Param sub page later
        self.request.session['selected_study_id'] = [self.object.pk]

        deidparams = DeidParams.objects.filter(studyId=o.id).order_by('paramName')
        logging.error(deidparams)
        context['deidparams'] = deidparams
        return context


class StudyCreate(CreateView):
    template_name = 'app/study_form.html'
    model = Study
    fields = ('study_name', 'irb_num', 'idsc', 'pi_fname', 'pi_lname', 'pi_email', 'template')
    success_url = reverse_lazy('study_list')


class StudyUpdate(UpdateView):
    template_name = 'app/study_form.html'
    model = Study
    fields = ('study_name', 'irb_num', 'idsc', 'pi_fname', 'pi_lname', 'pi_email', 'template')
    success_url = reverse_lazy('study_list')


class StudyDelete(DeleteView):
    template_name = 'app/study_confirm_delete.html'
    model = Study
    success_url = reverse_lazy('study_list')


class StudyView(FormView):
    model = Study

########################################################################
# DeidParam

class DeidParamsCreate(CreateView):
    template_name = 'app/deidparams_form.html'
    model = DeidParams
    fields = ('paramName', 'paramValue',)

    def get_success_url(self):
        study_id = self.request.session['selected_study_id'][0]
        logging.warning(f"Get_success_url {study_id} ")
        return reverse_lazy('study_detail', kwargs={'pk': study_id})

    def form_valid(self, form):
        """ We add in the Study before saving this parameter as saved in the session """
        id = self.request.session['selected_study_id'][0]
        logging.warning(f"ID = {id}")
        study = Study.objects.filter(pk=id)[0]
        logging.warning(f"study = {study}")
        form.instance.studyId = study

        return super().form_valid(form)

    def get_initial(self, *args, **kwargs):
        initial = super(DeidParamsCreate, self).get_initial(**kwargs)
        logging.warning("session found")

        logging.warning(self.request.session["selected_study_id"])
        selected_study_id = self.request.session["selected_study_id"]
        # how to set initial field values
        # initial['paramName'] = "George1"
        return initial


########################################################################
# DicomElements Classes
class DicomElementsList(ListView):
    template_name = 'app/dicomelements_details.html'
    logging.debug("in DicomElementsList")
    model = DicomElements


class DicomElementsDelete(DeleteView):
    template_name = 'app/dicomelements_confirm_delete.html'
    model = DicomElements
    success_url = reverse_lazy('dicomelements_list', )

class DicomElementsUpdate(UpdateView):
    template_name = 'app/dicomelements_form.html'
    model = DicomElements
    fields = ('element', 'name', 'defaultValue', 'checked')
    success_url = reverse_lazy('dicomelements_list')

class DicomElementsCreate(CreateView):
    template_name = 'app/dicomelements_form.html'
    model = DicomElements
    fields = ('element', 'name', 'defaultValue', 'checked')
    success_url = reverse_lazy('dicomelements_list')
    # def get_context_data(self, **kwargs):
    #     # Call the base implementation first to get a context
    #     context = super().get_context_data(**kwargs)
    #     # Add in a QuerySet of all the books
    #     context['subject_list'] = Study.objects.all()
    #     return context

########################################################################
# Deid  Templates

class DeidTemplateUpdate(UpdateView):
    template_name = 'app/deidtemplate_form.html'
    fields = ('templateName',)
    model = DeidTemplate
    success_url = reverse_lazy('deidtemplate_list')

class DeidTemplateList(ListView):
    template_name = 'app/deidtemplate_list.html'
    model = DeidTemplate

class DeidTemplateDetail(DetailView):
    template_name = 'app/deidtemplate_details.html'
    model = DeidTemplate

    def get_context_data(self, **kwargs):
        logging.warning(f"in get_context_data")
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        o = context['object']
        # Set this for use on the creation of Param sub page later
        self.request.session['selected_dicomelements_id'] = [self.object.pk]

        templateElements = TemplateElements.objects.filter(deidTemplate_id=o.id).order_by('element')
        logging.debug(templateElements)
        context['templateElements'] = templateElements
        return context

class DeidTemplateDelete(DeleteView):
    model = DeidTemplate
    template_name = 'app/deidTemplate_confirm_delete.html'
    success_url = reverse_lazy('deidtemplate_list')

class DeidTemplateCreate(CreateView):
    template_name = 'app/deidtemplate_form.html'
    fields = ('templateName','deidParams')
    model = DeidTemplate

    def get_success_url(self):
        return reverse_lazy('deidtemplate_list' )

    def get_initial(self, *args, **kwargs):
        initial = super(DeidTemplateCreate, self).get_initial(**kwargs)
        logging.warning(f"self { self.model.deidParams}")
        # Copy all defaultElements to this template as initial setup.
        for element in DicomElements.objects.all():
            logging.warning(f"element found {element}")
            #self.model.deidParams.add(element)

        logging.warning("session found")
    #
    # def form_valid(self, form):
    #     """ We add in the Study before saving this parameter as saved in the session """
    #     id = self.request.session['selected_study_id'][0]
    #     logging.warning(f"ID = {id}")
    #     study = Study.objects.filter(pk=id)[0]
    #     logging.warning(f"study = {study}")
    #     form.instance.studyId = study

# Create your views here. function based
# from django.http import HttpResponse
# from .models import Study
# Study Listing is the default
# def index(request):
#     study_count = Study.objects.order_by('study_name').count()
#     context = {
#         'study_count': study_count,
#     }
#
#     return   render(request, 'homepage.html', context)
#
# # Study Listing is the default
# def study_list(request):
#     study_list = Study.objects.order_by('study_name')
#     template = loader.get_template('study/subject_list.html')
#     context = {
#         'study_list': study_list,
#     }
#
#     return   render(request, 'study/subject_list.html', context)

# Study Details
# def study_detail(request , id):
#     post = get_object_or_404(Study, id=id )
#     return render(request,
#                   'study/detail.html',
#                   {'post': post})
