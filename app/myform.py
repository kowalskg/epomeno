from django import forms

class StudyForm(forms.Form):

    name = forms.CharField()
    message = forms.CharField(widget=forms.Textarea)

    def send_email(self):
        # send email using the self.cleaned_data dictionary
        pass

# class DeidParamsForm(forms.Form):
#     class Meta:
#         model = models.DeidParamsForm