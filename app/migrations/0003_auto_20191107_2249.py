# Generated by Django 2.2.7 on 2019-11-07 22:49

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_auto_20191107_2247'),
    ]

    operations = [
        migrations.RenameField(
            model_name='redcapaccession',
            old_name='redcap_request_id',
            new_name='redcap_request',
        ),
    ]
