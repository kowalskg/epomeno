from django.urls import path

from app.views import StudyList
from . import views
from . import myform

urlpatterns = [
    # path('', views.index, name='index'),
    # path('/app/study/', views.index ),
    path('', views.StudyList.as_view(), name="index"),

    path('study', views.StudyList.as_view(), name='study_list'),
    path('study/<int:pk>', views.StudyDetail.as_view(), name='study_detail'),
    path('study/create', views.StudyCreate.as_view(), name='study_create'),
    path('study/update/<int:pk>', views.StudyUpdate.as_view(), name='study_update'),
    path('study/delete/<int:pk>', views.StudyDelete.as_view(), name='study_delete'),

    path('deidParams/', views.DeidParamsCreate.as_view(), name='deidparam_list'),
    path('deidParams/create', views.DeidParamsCreate.as_view(), name='deidparam_create'),

    # Default Elements that are used to pre-populate a template
    path('dicomElements', views.DicomElementsList.as_view(), name='dicomelements_list'),
    path('dicomElements/<int:pk>', views.DicomElementsList.as_view(), name='dicomelements_detail'),
    path('dicomElements/create', views.DicomElementsCreate.as_view(), name='dicomelements_create'),
    path('dicomElements/update/<int:pk>', views.DicomElementsUpdate.as_view(), name='dicomelements_update'),
    path('dicomElements/delete/<int:pk>', views.DicomElementsDelete.as_view(), name='dicomelements_delete'),


    # templates
    path('deidTemplate', views.DeidTemplateList.as_view(), name='deidtemplate_list'),
    path('deidTemplate/<int:pk>', views.DeidTemplateDetail.as_view(), name='deidtemplate_detail'),
    path('deidTemplate/update/<int:pk>', views.DeidTemplateUpdate.as_view(), name='deidtemplate_update'),
    path('deidTemplate/create', views.DeidTemplateCreate.as_view(), name='deidtemplate_create'),
    path('deidTemplate/delete/<int:pk>', views.DeidTemplateDelete.as_view(), name='deidtemplate_delete'),

    # Template Elements
    # path('templateElements', views.TemplateElementsList.as_view(), name='templateElements_'),

# path('/app/study/<int:id>', views.study_detail, name='study_detail')
]
