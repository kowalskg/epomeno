#
# Django Extension Enable

from app.models import Study
from pyxnat import Interface
import os

def run(*args):


    interface = Interface(config="./sample/xnat.conf")
    #
    users =  interface.select.project("IDSC1000").users()
    print(f"Project :  {users}")
    interface.save_config("/tmp/myconfig.conf")

    mylist = interface.select.projects()
    for project in mylist :
        print ( f"Projects :  {project} : {project.attrs.get('ID') } : {project.attrs.get('secondary_ID') } :  {project.attrs.get('xnat:projectData/name') } ")
        # pi = project.attrs.get('xnat:projectData/PI')
        # print ( pi)
        idsc_num = project.attrs.get('ID')
        study_name = project.attrs.get('xnat:projectData/name')
        pi_fname =  project.attrs.get('xnat:projectData/PI/firstname')
        pi_lname =  project.attrs.get('xnat:projectData/PI/lastname')

        epo_study = Study.objects.filter(idsc__exact=idsc_num)
        if epo_study :
            print ( f"Study {idsc_num} already existed ... skipping . ")
        else:
            print(f"Creating Project {idsc_num}  ...  ")
            Study.objects.create( idsc=idsc_num, study_name= study_name , pi_fname = pi_fname, pi_lname = pi_lname)

    # print ("subject Fields :")
    # print ( interface.inspect.datatypes('xnat:subjectData'))
    # print ("Project Fields : ")
    # print(interface.inspect.datatypes('xnat:projectData'))
    # print("data types")



