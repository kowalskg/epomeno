# Generated by Django 2.2.7 on 2019-11-07 23:05

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_auto_20191107_2249'),
    ]

    operations = [
        migrations.AlterField(
            model_name='redcapaccession',
            name='created_date',
            field=models.TimeField(default=django.utils.timezone.now),
        ),
        migrations.AlterField(
            model_name='redcaprequest',
            name='date_submitted',
            field=models.TimeField(blank=True),
        ),
        migrations.AlterField(
            model_name='study',
            name='created_date',
            field=models.TimeField(default=django.utils.timezone.now),
        ),
    ]
