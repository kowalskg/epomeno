from django.db import models
from django.utils.timezone import now
import logging


# Create your models here.
class RedcapRequest(models.Model):
    record_src = models.CharField(max_length=1, blank=True)
    # i for initial  r for repeat and f for ferriscan
    record_id = models.IntegerField(blank=False)
    contact_fname = models.CharField(max_length=50, blank=True)
    contact_lname = models.CharField(max_length=50, blank=True)
    contact_email = models.CharField(max_length=50, blank=True)
    pi_fname = models.CharField(max_length=50, blank=True)
    pi_lname = models.CharField(max_length=50, blank=True)
    date_submitted = models.DateTimeField(blank=True)
    idsc = models.CharField(max_length=20, blank=True)
    notes = models.CharField(max_length=2000, blank=True)
    irb_num = models.CharField(max_length=255, blank=True)
    req_turn_time = models.CharField(max_length=255, blank=True)
    state = models.CharField(max_length=10, default='new', blank=True)
    created_date = models.DateTimeField(auto_now_add=True)

    def __repr__(self):
        return f"Request {self.record_src}:{self.record_id}"

    def __str__(self):
        return f"Request {self.record_src}:{self.record_id}"

    class Meta:
        db_table = "redcap_request"


class RedcapAccession(models.Model):
    redcap_request = models.ForeignKey('RedcapRequest', on_delete=models.CASCADE)
    accession = models.CharField(max_length=50, blank=True)
    modaility = models.CharField(max_length=255, blank=True)
    subject_deid = models.CharField(max_length=255, blank=True)
    studyinstance_uid = models.CharField(max_length=255, blank=True)
    state = models.CharField(max_length=10, default='new')
    created_date = models.DateTimeField(default=True)

    def __repr__(self):
        return self.accession

    def __str__(self):
        return self.accession

    class Meta:
        db_table = "redcap_accession"


class Study(models.Model):
    study_name = models.CharField(max_length=255)
    irb_num = models.CharField(max_length=20, blank=True)
    idsc = models.CharField(max_length=20, blank=True)
    pi_fname = models.CharField(max_length=50, blank=True)
    pi_lname = models.CharField(max_length=50, blank=True)
    pi_email = models.CharField(max_length=50, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    template = models.ForeignKey('DeidTemplate', on_delete=models.DO_NOTHING, null=True, blank=True)
    deidParams = models.ForeignKey('DeidParams', on_delete=models.DO_NOTHING, null=True, blank=True)

    def __repr__(self):
        return self.study_name

    def __str__(self):
        return self.study_name

    class Meta:
        db_table = "study"
        ordering = ["-study_name"]


class DeidTemplate(models.Model):
    templateName = models.CharField(max_length=255)
    created_date = models.DateTimeField(auto_now_add=True)
    deidParams = models.ForeignKey('TemplateElements', on_delete=models.DO_NOTHING, null=True, blank=True)

    def __repr__(self):
        return self.templateName

    def __str__(self):
        return self.templateName

    class Meta:
        db_table = "deid_template"


class DeidParams(models.Model):
    paramName = models.CharField(max_length=255)
    paramValue = models.CharField(max_length=255)
    studyId = models.ForeignKey('Study', on_delete=models.CASCADE, null=True)
    created_date = models.DateTimeField(auto_now_add=True)

    def __repr__(self):
        return self.paramName

    def __str__(self):
        return self.paramName

    class Meta:
        db_table = "deid_params"


class DicomElements(models.Model):
    checked = models.CharField(blank=True, null=True, max_length=1)
    element = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    defaultValue = models.CharField(max_length=255, null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)

    def __repr__(self):
        return f"{self.element} : {self.name}"

    def __str__(self):
        return f"{self.element} : {self.name}"

    class Meta:
        db_table = "default_dicom_elements"


class TemplateElements(models.Model):
    checked = models.BooleanField(blank=True, null=True)
    element = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    value = models.CharField(max_length=255, null=True, blank=True)
    deidTemplate = models.ForeignKey('DeidTemplate', on_delete=models.DO_NOTHING, null=True)
    created_date = models.DateTimeField(auto_now_add=True)

    def __repr__(self):
        return f"{self.element} : {self.name}"

    def __str__(self):
        return f"{self.element} : {self.name}"

    class Meta:
        db_table = "template_elements"
