#!env python
# Read the default dicom-anonymiser.script , which was part of the DicomanonymizerTool-installer.jar ( http://mirc.rsna.org/download )
# and pre-populate the idsc.sqlite DB with a set of default values
# 3/3030 - George Kowalski
# Does not clean out table. Run it after you first start up the DJango App and migrate the database


import xml.etree.ElementTree as ET
import sqlite3
import datetime

with  open("../sample/dicom-anonymizer.script") as f:
    read_data = f.read()

# print(read_data)

def parseXML(xmlfile):
    # create element tree object
    conn = sqlite3.connect('../idsc.sqlite')
    sqlInsert = "INSERT INTO default_dicom_elements (element, name,  defaultValue, created_date , checked) VALUES (?, ?,  ?, ?, ?); "
    now = datetime.datetime.now()
    # for row in conn.execute("select * from study"):
    #     print(row)

    tree = ET.parse(xmlfile)

    # get root element
    root = tree.getroot()

    # iterate project items
    print(f"Project Attributes ----------------------- {now}")
    for child in root.findall('p'):
       print(f"{child.tag} -> {child.attrib} \n" )

    print ("E Attributes -----------------------" )
    for child in root.findall('e'):
       print(f"{child.tag} -> {child.attrib['t']} -> {child.attrib['n']} -> {child.text} -> {child.attrib['en']} \n" )
       conn.execute(sqlInsert, ( child.attrib['t'], child.attrib['n'], child.text, now , child.attrib['en']))
    conn.commit()
    conn.close()

parseXML("../sample/dicom-anonymizer.script")