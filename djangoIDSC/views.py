from django.shortcuts import render, get_object_or_404
from django.template import loader

# Create your views here.
# TODO: Convert to CBV instead of FBV
from django.http import HttpResponse
from app.models import Study, DeidTemplate,   DicomElements

def homepage(request):
    study_count = Study.objects.order_by('study_name').count()
    default_dicom_element_count = DicomElements.objects.count()
    template_count = DeidTemplate.objects.count()
    template_elements_count = DeidTemplate.objects.count()
    context = {
        'study_count': study_count,
        'template_count': template_count,
        'template_elements_count': template_elements_count,
        'default_dicom_element_count': default_dicom_element_count
    }

    return   render(request, 'homepage.html', context)
