from django.contrib import admin

from .models import RedcapAccession, RedcapRequest, Study, DeidTemplate, DeidParams, DicomElements, TemplateElements

# Register your models here.

admin.site.register(RedcapAccession)
admin.site.register(RedcapRequest)
admin.site.register(DeidTemplate)
admin.site.register(DeidParams)
admin.site.register(DicomElements)
admin.site.register(TemplateElements)


@admin.register(Study)
class PostAdmin(admin.ModelAdmin):
    list_display = ('study_name', 'idsc', 'pi_fname', 'pi_lname')
    list_filter= (  'created_date',)
    search_fields = ('study_name', 'idsc', 'pi_lname')
    # date_hierarchy = 'created_date'